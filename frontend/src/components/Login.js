
import React, { useState } from 'react';
import axios from 'axios';
import "primereact/resources/themes/lara-light-indigo/theme.css"; 
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { useNavigate } from 'react-router-dom';

import { Card } from 'primereact/card';
import 'primeicons/primeicons.css';
        

        
function Login() {
    
    const [userid, setUserid] = useState('');
    const [password, setPassword] = useState('');
   // const history = useHistory();
    const navigate=useNavigate()
    const handleRegister=()=>{
        navigate('/LoginPage')
    }
    const handleSubmit = (event) => {
        event.preventDefault();
        axios.post(`http://localhost:5001/newlogin/${userid}`, {
            userid: userid,
            password: password
        })
        .then(response => {
            console.log(response)
            if (response.data === "success") {
                navigate('/NavPage', { state: { userid } });
            } else {
                alert('Invalid login credentials.');
            }
        })
        .catch(error => {
            console.log(error);
        });
    };

    return (
        <div >
            <Card style={{ width: '450px', margin: '0 auto' , marginTop:'150px'}}>
            <form onSubmit={handleSubmit}>
                <label >
                  <strong>Student id:</strong>  
                    <br /><br></br>
                    <InputText value={userid} onChange={(e) => setUserid(e.target.value)} />
                    <br />
                </label>
                <label>
                    <br />
                    <strong>Password:</strong>
                    <br /><br></br>
                    <InputText type='password' value={password} onChange={(e) => setPassword(e.target.value)} />
                    <br />
                    <br />
                </label>
                <br />
                <Button label="Login" icon="pi pi-user"  aria-label="User" style={{ width: '130px' }}/>
                <br /><br />
                
            </form>
            <Button label="Register"onClick={handleRegister} icon="pi pi-user-plus" style={{ width: '130px' }}/>
            </Card>
        </div>
    );
}

export default Login;