import React from 'react'
import { useState } from 'react';
import { useEffect } from 'react';
import axios from 'axios';


function UpdatedFile() {
  const [data, setData] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get('http://localhost:5001/Updated_file');
      setData(response.data);
    } catch (error) {
      console.error('Error fetching dataa:', error);
    }
  };

  return (
    <div>
      <h1>HELLO UPDATED FILE</h1>
      <table>
        <thead>
          <tr>
            <th>NAME</th>
            <th>EMAIL</th>
            <th>MOBILE</th>
            <th>ATTENDANCE</th>
          </tr>
        </thead>
        <tbody>
          {data.map((item) => (
            <tr key={item.id}>
              <td>{item.name}</td>
              <td>{item.emaill}</td>
              <td>{item.phone}</td>
              <td>{item.attendance}</td>
             
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default UpdatedFile;
